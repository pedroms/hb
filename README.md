# Remote.com handbook

Locally (renders drafts):

```
hugo server -D
```

If you're using VSCode, consider installing the Hugo extension to make HTML format better.

## Organization of content

Right now, all handbook content is found in `content/handbook/general.md`.

Any new files you add, will just be rendered below that.

## Theme and style

We use a custom, super simple Hugo theme, that is adapted from the one running on
jobvandervoort.com (i.e. I copied the css).

Find all css in `themes/remote_handbook/assets/scss/all.scss`.

The TOC is rendered in `themes/remote_handbook/layouts/_default/baseof.html`.

The rest of the rendering happens in `themes/remote_handbook/layouts/index.html`.

## Deployment

The handbook is deployed through Netlify.

Note that you need to compile all assets locally (happens automatically if you run a local server),
Netlify is not able yet to compile hugo/extended, which is necessary for SCSS compilation.