---
title: "General"
date: 2019-03-11
draft: false
---

# General

This is the handbook for Remote.com: it's how we run our company.
You're free to reference or copy any part of it, as long as you refer back to us.

This handbook is a living document. Please make changes to it,
[find the repository here](https://gitlab.com/remote-com/hb).

## Our values

### Kindness

In all your communication and decision making, be kind.
That means assuming good intent in all situations, doing extra work to
help out others.

We're building Remote.com to make the lives of people better,
a single kind gesture - or simply treating people well, can have
a meaningfully positive impact on their lives.

#### What if someone isn't kind?

Assume it's not bad intent and simply ask for clarification,
in case someone is being short or brash. It can simply be a bug
in communication.

If you're not comfortable with that, or someone is intentionally
being unkind, reach out to your direct manager.

### Ownership

Remote.com is the product of all its contributors, not a brainchild of its management.
This means that you're expected to treat it as your own: act in the best benefit of Remote.com,
take ownership of any work you're doing: don't just do as you're being told.

Your ownership extends to every part of our company. If you think we should do something different,
even if it's in a completely different area of the company than the one you're active in,
make a change! This goes from this handbook, to our processes, to our core strategy and product.
Everything is mutable.

We trust you. If you think we should be listed somewhere, have an account on any service that will
benefit Remote.com in some way, just do it! Don't ask for permission, we believe in you and that your actions
and decisions will benefit the company. Make it public what you're doing and share the account details (with a strong password) in 1Password.

#### Independence

It's up to you to decide how you do your job at Remote. This works two ways:

1. You're free in deciding, when, where and using what tools you do your job.
1. You're expected to always think independently. Always speak up if you have reservations about something, work in a way that you think works best. Take initiative.

### Delight

We want to delight our customers at every step, no matter what happens.
We are to be obsessed with their happiness and success, and even surprise them (in a good way)
when possible.

Examples of this are:

1. Amazing support. Quick, personal, and helpful.
1. Delight in our product. We're making hard, boring things, easy. Let's try to make them fun.
1. Delight in our communication. If we sound corporate, we're doing it wrong. See the Twitter feed by Slack
   as a good example. It's quirky, fun, responsive, but still very helpful.

## Expectations

Working for Remote is just a job, like you’ve probably had before and will probably have after. It is not your entire life, and please don’t make it so.

If you want to take time off, for any reason, please do so. You don't have to give a reason.
We're not keeping track of your days off either.

It’s your managers job to make clear what you’re expected to do.
You manage your own time. If you have to work more than 40 hours a week to do a satisfactory job
(be that in your own eyes or someone else’s), something is wrong. Discuss it with your manager
as soon as possible, and they will help you move forward.

If for any reason your manager is not happy with your work,
they will tell you immediately, in private.

If you’re not happy with your work, for any reason at all, share that with your manager at the earliest opportunity. This way `we can work out something where everyone benefits.

## Vacation and sick leave policy

You have unlimited vacation days. In practice this means you don’t have to ask for permission when planning vacation. Do inform your manager and peers, and make sure your responsibilities are covered.

We do not keep a log of days taken off, but to be able to quickly see upcoming time off,
please add [to this note in Notion][vacation-notion].
Days that have passed will be removed.

Your and the health of your loved ones comes before work. If you can, inform your manager if you have to take time off for health reasons.

### Public holidays

Given the policy above, it's up to you to work on national / public holidays.
You can take the day off, you can work. It's up to you.

If your manager or colleague is working on a public holiday, that doesn't mean you should.

It's not a given that everyone celebrates the same public holidays.
So, even in the case of taking a day off on a public holiday, add this [to the note in Notion][vacation-notion] and let your manager know.

[vacation-notion]: https://www.notion.so/remotecom/a025ae1977e3413e8a7af0b82e5139b7?v=351eb6c614184daf947fe357e0617832

### Taking care of yourself

It's important that you're healthy and happy. Taking days off work is a good way to stay both.

It is your responsibility to take sufficient days off to remain healthy and happy. If you have
no current plans for taking days off, that's a signal that you should plan to take time off.

If you manage people, make sure to check on this as well.

## Behavior and language

1. Do not make jokes or unfriendly remarks about race, ethnic origin, skin color, gender, or sexual orientation.
2. Use inclusive language. For example, prefer "Hi everybody" or "Hi people" to "Hi guys".
3. Share problems you run into, ask for help, be forthcoming with information and speak up.
4. Don't display surprise when people say they don't know something, as it is important that everyone feels comfortable saying "I don't know" and "I don't understand." (As inspired by Recurse.)
5. You can always refuse to deal with people who treat you badly and get out of situations that make you feel uncomfortable.
6. Always be kind.

Everyone can remind anyone in the company about these guidelines. If there is a disagreement about the interpretations, the discussion can be escalated to more people within the company without repercussions.
If you are unhappy with anything (your duties, your colleague, your boss, your salary, your location, your computer) please let your boss, or the CEO, know as soon as you realize it. We want to solve problems while they are small.
Make a conscious effort to recognize the constraints of others within the team. For example, sales is hard because you are dependent on another organization, and Development is hard because you have to preserve the ability to quickly improve the product in the future.

## Communication

Always prefer async communication over sync communication. Sync communication is costly, interrupts peoples' thought processes and productive execution - that being said, sometimes it is indeed necessary to have a sync interaction, just make sure it is for the right reasons.

When using Slack or similar, try to communicate as much as possible in channels that everyone can access rather than private channels, whenever that can be avoided. We prime visibility and open dialog.

## Meetings

[Read this blog post on how to run a successful meeting](https://blog.remote.com/how-to-run-a-successful-meeting/).

## Expense policy

We have no VC funding, so we have to be frugal. Ask your manager if there's something
you want to expense. E.g. travel to a company meetup is reasonable to expense,
as long as you don't take a limousine.

You don't have to ask permission to expense:

1. Coworking space costs up to 150 euro / month

# Product

** This is a work in progress, please help out here!**

## Accessibility

One of the awesome advantages of remote work is the ability to level
the playing field for people with disabilities.

To be able to serve everyone equally well, everything we produce should be
accessible for everyone. That means we have to make sure the things we build are:

- consumable from any (supported) device: mobile, desktop
- usable with screen readers
- accessible in terms of readibility: allow resizing. Don't use low-contrast text for important interface elements
- low bandwidths: not everyone always has access to highspeed internet

There are many resources online that provide a much better overview of this.
See e.g. [www.w3.org/WAI/fundamentals/accessibility-intro/](https://www.w3.org/WAI/fundamentals/accessibility-intro/)

## Interactivity

Elements that are interactable, should show that. This means that buttons and links that are active:

- Set a pointer cursor
- Have a hover-over state
- Links have a visited state

## Written

For UI text, use sentence case: The first letter of the first word should be capitalized.

- Correct: `Find a job`
- Incorrect: `Find A Job`, `Find a Job`, `find a job`

# Branding

[See this page](/branding).

# Product Development and Engineering

## Methodology

We keep it simple, maximize productivity and reduce waste.

Development should be about:

- Small changes.
- Iterate quickly.
- Deploy often.
- Simple, well written and readable code.
- Doing things that move the needle.
- Perferring to ship 1 task than half-assing 2.

### Flow

Working asynchronously also means you don't need to wait for other people to assign you work.
There will always be a backlog of work and if there isn't you should use your better judgement and pick up whatever you think has most impact/benefit. If there isn't any work planned you should bring it up with your manager, there should always be a plan.

As all work should be written down in issues, if you're doing something out of your own volition and no one shared a spec (tech or product) with you, then create an issue for it before get going.

We use [Kanban (看板) (signboard or billboard in Japanese)](https://en.wikipedia.org/wiki/Kanban) to map the different status of work.
Think of it as pipeline process flowing from left to right, from concept to execution.

```
    Open --> ToDo --> Doing --> Done --> Closed
```

**_Status:_**

- **_Open_** - Initial status of an issue, it's `unassigned`, possibly yet to be `prioritized` and `planned`. The issue creator should make sure that it gets the proper visibility, use the issue thread for it.
- **_ToDo_** - Issue has been `planned`, is now `assigned` and has been `prioritized`.
- **_Doing_** - Assignee is working on the issue. There shouldn't be many issues with the same assignee in Doing. Remember to keep it flowing, if there are many issues, it means no work is being finished. Always prefer to [create a branch out of the issue](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch-from-an-issue) for traceability purposes.
- **_Done_** - Assignee has finished the task and there's a Merge Request created and waiting to be merged. This keeps the kanban clean. The assignee is still responsible for the issue and should make sure it gets accepted and deployed.
- **_Closed_** - The assignee's task is deployed to production (and validated). When finishing a task provide a comment on what was done. There should be a link to the MR.

**_Pay attention to:_**

- Never let issues pile up and go stale.
- Don't work on many things at the same time. Keep it simple, keep it focused.
- Clearly communicate if you need help from someone to move it along the board, use the issue thread for it.
- When assigning a task it should have one single owner (assignee). Others can be tagged and mentioned but the owner is responsible for pushing it forward. Owners can change.
- You never want to stop the flow, an issue that stays too long in one state (rather than Closed) means there's something wrong with it.

# Community

Great products are built on years of feedback from its users.
It's of vital importance to building a strong company, product and brand that we grow and maintain a great community.

Communities are built on mutual trust. As a company we have to earn that by being transparent, fair, and by showing
we respond to feedback good and bad.

# Hiring

Applying to a job is stressful and can be frustrating for applicants.
Plus, the balance of power is strongly skewed towards the employer.
This means people interviewing are typically not the best representation of themselves.
Success in interviewing does not necessarily predict success on the job.

Therefore, interviewing with Remote.com should be as comfortable and easy as possible.
Some guidelines:

1. Always be transparent with the candidate.
   1. If you realize early on that you won't be able to hire
      someone, just let them know. This is not common practice, because it's hard to be that direct. However,
      most people will appreciate you being candid.
   1. Don't make promises you can't keep. Something that happens often is a candidate asking whether a function
      can lead into a more senior role. You should have already thought about this before the interview, and often
      the response is "it depends". Candidates will understand it when you say that it depends on how they perform on the job.
1. Prepare and take time for interviews. If you rush someone, or don't have the time to prepare or finish you notes after the interview, it's better to not have the interview.
1. Prepare the candidate for the interview. If you expect them to e.g. go through work, do an exercise or anything
   else, don't surprise them with this. Let them know at the time of scheduling the interview.
1. It's okay to not be sure about a candidate, and ask for another interview.

## Process

Given we're a small team, everyone will be interviewed by both founders (Job + Marcelo) individually.

At shortest, the process looks like:

1. Interview with hiring manager (either founder)
1. Interview with other founder
1. Reference checks
1. Offer

We maintain a two-week trial period for which candidates will be paid normally.

# Marketing operations

## Sending newsletter

Before sending:

- Leave a message in support channel alerting the team

# Address

If you need an address for some reason, there's a mailbox here:

18 Bartol St. #1128 San Francisco, CA 94133

# Remarks

Part of the content is adapted from the [GitLab Inc Handbook](https://about.gitlab.com/handbook/).
If anything is missing in our handbook, that’s a good place to start from.
