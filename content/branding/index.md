---
title: "Branding"
date: 2019-03-10
draft: false
type: branding
---

# Brand assets

⬇️ Use our [GitLab repository][branding-project] to download the approved brand
assets, such as logos and colors. Please don't alter them in any way.

# Naming

The Remote.com name is one of our greatest assets: clear, concise, and
memorable. But we can also be easily mistaken if our name isn't used properly
and consistently. Please follow these guidelines when mentioning Remote.com in
your copy.

## ✅ Naming do's

- Display the word “Remote.com” in the same font size and style as its
  neighboring content.
- Capitalize the word “Remote.com,” except when it’s part of a web address.
- In code, filenames, aliases, handles, or URLs, use `remote-com`, `remotecom`,
  or `RemoteCom`.

## ⛔️ Naming dont's

- Don't remove the “.com” as it's an integral part of the name.
- Don’t pluralize the Remote.com trademark, use it as a verb or abbreviate it.
- Don’t use the Remote.com logo or symbol in place of words.

# Logo and symbol

The Remote.com logo and symbol are our personal signatures across all our
communications. They should be instantly recognizable, so consistency is key.
Please use the logo and symbol assets as provided, don’t edit, change, distort,
recolor, or reconfigure them in any way. Those are the only approved versions.

## Logo

Our primary signature is the Remote.com logo, that includes both the **“R”
symbol** and **“Remote.com” wordmark**. Try to use it for all mentions of
Remote.com to retain consistent brand recognition. The “Remote.com”
wordmark must be always accompanied by the symbol to form the logo.

![](logo.png)

## Symbol

The “R” symbol can be used as a supporting image and when there's a clear
association with Remote.com. But never by itself, unless the Remote.com logo or
name is used elsewhere in the communication.

![](symbol.png)

## Logo and symbol colors

Whenever possible, use our negative color logo or our white symbol over a Simple
Green background. Else, use our positive color logo or our Simple Green symbol
over a white background. Other colors, like black, or grayscale versions, are
ideal when placing them over off-brand colors or complex backgrounds like
photos.

![](logo-colors.gif)

## Clearance

Try to keep the clear space around the logo and symbol equal to the **height of
the symbol**. Depending on the type of communication and use, the clearance can
be smaller.

![](clearance.png)

## Scale

Our logo and symbol are designed to scale to small sizes on screen and print.
Minimum height: **12 pixels tall/0.42 centimeter tall/0.17 inch tall**.

![](scale.png)

# Color

In our [GitLab repository][branding-project] you can download the color palette
in various formats.

| Name | Chip | HEX | RGB | CMYK | PMS |
|------|------|-----|-----|------|-----|
| **Primary** | | | | | |
| Simple Green | <div class="color-chip" style="background: #27A465"></div> | 27A465 | 39 164 101 | 77 5 75 0 | 7481 |
| **Secondary** | | | | | |
| Happy Green | <div class="color-chip" style="background: #39F194"></div> | 39F194 | 57 241 148 | 44 0 40 0 | 352 |
| Warm Green | <div class="color-chip" style="background: #024E35"></div> | 024E35 | 2 78 53 | 91 40 81 45 | 3305 |

## Color proportions

**Simple Green** is the most important brand color. **White** follows it,
playing an important role in balancing the other colors and providing a simple
and uncomplicated experience. Finally, **Happy Green** and **Warm Green** should
be used sparingly as accent colors.

![](color-proportions.png)

# Typography

For non-interactive communications (e.g. printed materials, presentations,
bitmap images), try to use [**TT
Commons**](https://typetype.org/fonts/commons/), the base typeface of our
wordmark. Whenever possible, enable its stylistic alternates for more
approachable lowercase A's, L's, and Y's.

For interactive communications (e.g. web):

- Titles, headlines, and sub-heads use
  [**Montserrat**](https://github.com/JulietaUla/Montserrat) (available on
  [Google Fonts](https://fonts.google.com/specimen/Montserrat) or [Font
  Squirrel](https://www.fontsquirrel.com/fonts/montserrat)).
- Body copy, paragraphs, and labels use [**Fira
  Sans**](https://github.com/mozilla/Fira) (available on [Google
  Fonts](https://fonts.google.com/specimen/Fira+Sans) or [Font
  Squirrel](https://www.fontsquirrel.com/fonts/fira-sans)).

## ⛔️ Typography don'ts

Basic, good typography rules apply. But specifically, try to follow these:

- Don't use all caps.
- Don't use the same font weight for different hierarchy levels.

# Elements

- **Tagline**: Work better, live better.
- **Promise**: To help people live better lives, and companies work more
  efficiently through remote work.
- **Mission**: To be the single best place for anyone to start working remotely
  or get better at it, anywhere in the world. We're focused on changing people's
  lives and the way companies work, not just selling job ads.
- **Positioning**: We're more than a job listing website. We're a remote work
  platform, built by remote workers, for remote workers.
  - It's in our strategy: We invest in the content and tools that people and
    companies need to start, continue, and improve the way they work remotely.
  - It's in our name: As Remote.com, we want to become synonymous with “where
    remote work happens.”
  - It's in our personality: meet [Zoe, our design persona](#design-persona).
  - It's who we are: We came together to build Remote.com because we strongly
    believe in the benefits of remote work and want everyone to be able to
    experience them. We're a remote-only company, completely distributed with
    zero offices. We couldn't do it any other way.

# Design persona

## Overview

Zoe (from the Greek word for “life”), is the woman that embodies Remote.com's
personality. Her mantra is to celebrate the “good” life, the happy, healthy, and
meaningful life, by letting go of all things that aren't important — like that
daily commuting.

She's selfless, putting everyone else first. She is genuinely interested in
helping others lead better lives. In the difficult times, instead of a
handkerchief she greets you with a pocket full of remote work tips.

Although she has great aspirations, Zoe is simple, uncomplicated, humble, and
informal. A pair of jeans and a t-shirt is her favorite dress code.

Wherever she goes, Zoe always greets everyone with a kind smile. Her friends say
she's a master of dad jokes, the ones you just can't avoid laughing to.

Zoe is genuine, outspoken and not afraid of criticism. She would rather lose a
friend for being herself than to be vanilla. Even so, she is the first to
recognize her mess and be practical about it.

She dreads monotony and traditional stuff. Every year she does one big trip to
experiment with new ways of working, meet new people, and practice her Dutch and
Portuguese.

## Personality traits

- **Sincere, opinionated, and outspoken** but not rude or ignorant
- **Flexible, open-minded, and innovative** but not unstable or irrational
- **Warm, kind, and outgoing** but not fake or desperate
- **Practical, clear, and realistic** but not emotionless or impersonal
- **Genuine and open** but not ambiguous or vague

## Voice

Zoe's voice is informal, familar, and very approachable — no way she uses weird
acronyms and technical lingo just to make her sound smarter. She is there with
you in the good and bad, doesn't assume anything, and uses inclusive terms.

She talks with everyone as if they were her oldest friends, cracking up those
famous dad jokes and addressing them by their first name (or their embarrasing
high-school nickname).

When there is a serious issue, she's not afraid to get down to business
(remember Bob's intervention last year?) and spell it out concisely and clearly
in a way everyone can understand.

Most of all, Zoe is as human as a human can be. She loves telling stories, using
contractions like “don't” and sound effects like “baaam!”.

### Response examples

- **Greeting/welcome**: Hi Jane! Let's find the remote job you deserve.
- **Success feedback**: Awesomesauce! Your resume has been imported and is
  already looking better.
- **User error feedback**: Hold on, don't forget your address.
- **System error (non-critical)**: We didn't find any results yet. Please try
  something else.
- **System failure (critical)**: Oops! Don't panic, one of our servers is
  temporarily down but our engineers are on it. Please try again and if it still
  isn't working please wait a bit while we fix it. You can always reach us at
  @remote

## Engagement methods

- **Surprise and delight**: Confetti when something great, rare has happened.; A
  little ”poof” animation when you delete something. A rocket taking off after
  launching a new job post. Cool UI themes like 90s internet, hacker (all black
  and green, just text and ASCII), christmas, dark. Animated logo that changes
  depending on circumstances or actions.
- **Anticipation**: "Did you know that on average remote workers save X hours
  per year in commute?" - random remote work fact. Coundown to important / fun
  things. Thanks to specific companies or people that contributed. Highlight of
  non-serious benefits of remote work (you're always at home to receive
  packages).
- **Rewards**: Give aways: custom keyboard, a great set of pantufas, a donation
  to a charity of your choice.

[branding-project]: https://gitlab.com/remote-com/branding
